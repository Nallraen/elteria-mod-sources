package fr.em.nallraen.init;

import fr.em.nallraen.gui.MainMenu;
import fr.em.nallraen.gui.PauseMenu;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class MenuInit 
{

	public static final MenuInit INSTANCE = new MenuInit();
	
	@SubscribeEvent
	public void onOpenGui(GuiOpenEvent e)
	{
		if(e.getGui() != null && e.getGui().getClass() == GuiMainMenu.class)
		{
			e.setGui(new MainMenu());
		} 
		else if(e.getGui() != null && e.getGui().getClass() == GuiIngameMenu.class)
		{
			e.setGui(new PauseMenu());
		}
	}
}
