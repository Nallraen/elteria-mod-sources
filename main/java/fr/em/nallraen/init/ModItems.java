package fr.em.nallraen.init;


import java.util.List;

import com.google.common.collect.Lists;

import fr.em.nallraen.objects.items.EMItems;
import fr.em.nallraen.objects.items.EMRpgItem;
import fr.em.nallraen.util.Reference;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@EventBusSubscriber(modid = Reference.MOD_ID)
public class ModItems 
{
	
	public static final ModItems INSTANCE = new ModItems();
	
	public List<Item> items;
	
	// Items
	public static Item obsidian_ingot;
	
	public static Item uranium;
	public static Item elterite;
	public static Item pyrargyrite;
	public static Item aquamarine;
	public static Item tourmaline;
	public static Item amethyst;
	public static Item topaz;
	public static Item ruby;
	public static Item saphir;
	
	// RPG and Skills related items
	
	public static Item RPG_Item;
	
	
	
	public void init()
	{
		
		items = Lists.newArrayList();
		
		// Items
		amethyst = new EMItems("amethyst");
		obsidian_ingot = new EMItems("obsidian_ingot");
		
		uranium = new EMItems("uranium");
		elterite = new EMItems("elterite");
		pyrargyrite = new EMItems("pyrargyrite");
		aquamarine = new EMItems("aquamarine");
		tourmaline = new EMItems("tourmaline");
		
		topaz = new EMItems("topaz");
		ruby = new EMItems("ruby");
		saphir = new EMItems("saphir");
		
		// RPG Items
		
		RPG_Item = new EMRpgItem("rpg_item");
		
	}
	
	@SubscribeEvent
	public void registerModels(ModelRegistryEvent e)
	{
		for(Item item : items)
		{
			registerModel(item);
		}
	}
	
	
	@SideOnly(Side.CLIENT)
	private void registerModel(Item item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(new ResourceLocation(Reference.MOD_ID, item.getUnlocalizedName().substring(5)), "inventory"));
	}
	
	public List<Item> getItems()
	{
		return items;
	}
	
}
