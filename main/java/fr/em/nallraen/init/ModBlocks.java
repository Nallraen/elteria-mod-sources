package fr.em.nallraen.init;

import java.util.List;

import com.google.common.collect.Lists;

import fr.em.nallraen.objects.blocks.EMBlock;
import fr.em.nallraen.util.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModBlocks 
{
	
	public static final ModBlocks INSTANCE = new ModBlocks();
	
	
	// Blocks
	public static Block ruby_block;
	public static Block saphir_block;
	public static Block topaz_block;
	public static Block tourmaline_block;
	public static Block aqua_block;
	public static Block amethyst_block;
	public static Block ura_block;
	public static Block elterite_block;
	public static Block pyra_block;
	
	// Ores
	public static Block amethyst_ore;
	public static Block aqua_ore;
	public static Block pyra_ore;
	public static Block ruby_ore;
	public static Block saphir_ore;
	public static Block topaz_ore;
	public static Block ura_ore;
	public static Block tourmaline_ore;
	public static Block elterite_ore;
	
	
	private List<Block> blocks;
	
	
	public void init()
	{
		// Init list
		blocks = Lists.newArrayList();
		
		// Blocks
		ruby_block = new EMBlock("ruby_block", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		saphir_block = new EMBlock("saphir_block", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		topaz_block = new EMBlock("topaz_block", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		tourmaline_block = new EMBlock("tourmaline_block", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		aqua_block = new EMBlock("aqua_block", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		amethyst_block = new EMBlock("amethyst_block", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		ura_block = new EMBlock("ura_block", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		pyra_block = new EMBlock("pyra_block", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		elterite_block = new EMBlock("elterite_block", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		
		// Ores
		amethyst_ore = new EMBlock("amethyst_ore", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		aqua_ore = new EMBlock("aqua_ore", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		pyra_ore = new EMBlock("pyra_ore", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		ruby_ore = new EMBlock("ruby_ore", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		saphir_ore = new EMBlock("saphir_ore", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		topaz_ore = new EMBlock("topaz_ore", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		ura_ore = new EMBlock("ura_ore", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		tourmaline_ore = new EMBlock("tourmaline_ore", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		elterite_ore = new EMBlock("elterite_ore", Material.IRON, 5.0f, 30.0f, 2, "pickaxe");
		
		
		for(Block b : blocks)
		{
			ItemBlock ib = new ItemBlock(b);
			ib.setRegistryName(b.getRegistryName());
			GameRegistry.findRegistry(Item.class).register(ib);
		}
	}
	
	@SubscribeEvent
	public void registerModels(ModelRegistryEvent e)
	{
		for(Block b : blocks)
		{
			registerModel(b);
		}
	}
	
	private void registerModel(Block block)
	{
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), 0, new ModelResourceLocation(new ResourceLocation(Reference.MOD_ID, block.getUnlocalizedName().substring(5)), "inventory"));
	}
	
	public List<Block> getBlocks()
	{
		return blocks;
	}

}
