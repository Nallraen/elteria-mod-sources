package fr.em.nallraen.init;

import java.util.List;

import com.google.common.collect.Lists;

import fr.em.nallraen.objects.tools.EMAxe;
import fr.em.nallraen.objects.tools.EMHoe;
import fr.em.nallraen.objects.tools.EMPickAxe;
import fr.em.nallraen.objects.tools.EMSpade;
import fr.em.nallraen.util.Reference;
import fr.em.nallraen.util.ToolMaterials;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemSpade;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModTools 
{
	
	public static final ModTools INSTANCE = new ModTools();
	
	public List<ItemPickaxe> pa;
	public List<ItemHoe> hoe;
	public List<ItemAxe> axe;
	public List<ItemSpade> spade;
	
	// Tools
	
	// PickAxes
	
	public static ItemPickaxe ruby_pickaxe;
	public static ItemPickaxe topaz_pickaxe;
	public static ItemPickaxe ura_pickaxe;
	public static ItemPickaxe saphir_pickaxe;
	public static ItemPickaxe elterite_pickaxe;
	public static ItemPickaxe pyra_pickaxe;
	public static ItemPickaxe aqua_pickaxe;
	public static ItemPickaxe amethyst_pickaxe;
	public static ItemPickaxe tour_pickaxe;
	public static ItemPickaxe obsi_pickaxe;
	public static ItemPickaxe emerald_pickaxe;
	
	// Axes
	
	public static ItemAxe ruby_axe;
	public static ItemAxe topaz_axe;
	public static ItemAxe ura_axe;
	public static ItemAxe saphir_axe;
	public static ItemAxe elterite_axe;
	public static ItemAxe pyra_axe;
	public static ItemAxe aqua_axe;
	public static ItemAxe amethyst_axe;
	public static ItemAxe tour_axe;
	public static ItemAxe obsi_axe;
	public static ItemAxe emerald_axe;
	
	
	// Spades
	
	public static ItemSpade ruby_spade;
	public static ItemSpade topaz_spade;
	public static ItemSpade ura_spade;
	public static ItemSpade saphir_spade;
	public static ItemSpade elterite_spade;
	public static ItemSpade pyra_spade;
	public static ItemSpade aqua_spade;
	public static ItemSpade amethyst_spade;
	public static ItemSpade tour_spade;
	public static ItemSpade obsi_spade;
	public static ItemSpade emerald_spade;
	
	
	// Hoes
	
	public static ItemHoe ruby_hoe;
	public static ItemHoe topaz_hoe;
	public static ItemHoe ura_hoe;
	public static ItemHoe saphir_hoe;
	public static ItemHoe elterite_hoe;
	public static ItemHoe pyra_hoe;
	public static ItemHoe aqua_hoe;
	public static ItemHoe amethyst_hoe;
	public static ItemHoe tour_hoe;
	public static ItemHoe obsi_hoe;
	public static ItemHoe emerald_hoe;
	
	
	public void init()
	{
		pa = Lists.newArrayList();
		hoe = Lists.newArrayList();
		axe = Lists.newArrayList();
		spade = Lists.newArrayList();
		
		// Tools
		
		// Pickaxe Tools
		
		ruby_pickaxe = new EMPickAxe("ruby_pickaxe", ToolMaterials.rubyMat);
		topaz_pickaxe = new EMPickAxe("topaz_pickaxe", ToolMaterials.topazMat);
		ura_pickaxe = new EMPickAxe("ura_pickaxe", ToolMaterials.uraMat);
		saphir_pickaxe = new EMPickAxe("saphir_pickaxe", ToolMaterials.saphirMat);
		elterite_pickaxe = new EMPickAxe("elterite_pickaxe", ToolMaterials.elteriteMat);
		pyra_pickaxe = new EMPickAxe("pyra_pickaxe", ToolMaterials.pyraMat);
		aqua_pickaxe = new EMPickAxe("aqua_pickaxe", ToolMaterials.aquaMat);
		amethyst_pickaxe = new EMPickAxe("amethyst_pickaxe", ToolMaterials.amethystMat);
		tour_pickaxe = new EMPickAxe("tour_pickaxe", ToolMaterials.tourMat);
		obsi_pickaxe = new EMPickAxe("obsi_pickaxe", ToolMaterials.obsiMat);
		emerald_pickaxe = new EMPickAxe("emerald_pickaxe", ToolMaterials.emeraldMat);
		
		// Axe Tools
		
		ruby_axe = new EMAxe("ruby_axe", ToolMaterials.rubyMat, 4.0f, 1.5f);
		topaz_axe = new EMAxe("topaz_axe", ToolMaterials.topazMat, 4.0f, 1.5f);
		ura_axe = new EMAxe("ura_axe", ToolMaterials.uraMat, 4.0f, 1.5f);
		saphir_axe = new EMAxe("saphir_axe", ToolMaterials.saphirMat, 4.0f, 1.5f);
		elterite_axe = new EMAxe("elterite_axe", ToolMaterials.elteriteMat, 4.0f, 1.5f);
		pyra_axe = new EMAxe("pyra_axe", ToolMaterials.pyraMat, 4.0f, 1.5f);
		aqua_axe = new EMAxe("aqua_axe", ToolMaterials.aquaMat, 4.0f, 1.5f);
		amethyst_axe = new EMAxe("amethyst_axe", ToolMaterials.amethystMat, 4.0f, 1.5f);
		tour_axe = new EMAxe("tour_axe", ToolMaterials.tourMat, 4.0f, 1.5f);
		obsi_axe = new EMAxe("obsi_axe", ToolMaterials.obsiMat, 4.0f, 1.5f);
		emerald_axe = new EMAxe("emerald_axe", ToolMaterials.emeraldMat, 4.0f, 1.5f);
		
		// Spades Tools
		
		ruby_spade = new EMSpade("ruby_spade", ToolMaterials.rubyMat);
		topaz_spade = new EMSpade("topaz_spade", ToolMaterials.topazMat);
		ura_spade = new EMSpade("ura_spade", ToolMaterials.uraMat);
		saphir_spade = new EMSpade("saphir_spade", ToolMaterials.saphirMat);
		elterite_spade = new EMSpade("elterite_spade", ToolMaterials.elteriteMat);
		pyra_spade = new EMSpade("pyra_spade", ToolMaterials.pyraMat);
		aqua_spade = new EMSpade("aqua_spade", ToolMaterials.aquaMat);
		amethyst_spade = new EMSpade("amethyst_spade", ToolMaterials.amethystMat);
		tour_spade = new EMSpade("tour_spade", ToolMaterials.tourMat);
		obsi_spade = new EMSpade("obsi_spade", ToolMaterials.obsiMat);
		emerald_spade = new EMSpade("emerald_spade", ToolMaterials.emeraldMat);
		
		// Hoe Tools
		
		ruby_hoe = new EMHoe("ruby_hoe", ToolMaterials.rubyMat);
		topaz_hoe = new EMHoe("topaz_hoe", ToolMaterials.topazMat);
		ura_hoe = new EMHoe("ura_hoe", ToolMaterials.uraMat);
		saphir_hoe = new EMHoe("saphir_hoe", ToolMaterials.saphirMat);
		elterite_hoe = new EMHoe("elterite_hoe", ToolMaterials.elteriteMat);
		pyra_hoe = new EMHoe("pyra_hoe", ToolMaterials.pyraMat);
		aqua_hoe = new EMHoe("aqua_hoe", ToolMaterials.aquaMat);
		amethyst_hoe = new EMHoe("amethyst_hoe", ToolMaterials.amethystMat);
		tour_hoe = new EMHoe("tour_hoe", ToolMaterials.tourMat);
		obsi_hoe = new EMHoe("obsi_hoe", ToolMaterials.obsiMat);
		emerald_hoe = new EMHoe("emerald_hoe", ToolMaterials.emeraldMat);
	}
	
	
	@SubscribeEvent
	public void registerModels(ModelRegistryEvent e)
	{
		for(ItemPickaxe item : pa)
		{
			registerModelPickaxe(item);
		}
		for(ItemHoe item : hoe)
		{
			registerModelHoe(item);
		}
		for(ItemAxe item : axe)
		{
			registerModelAxe(item);
		}
		for(ItemSpade item : spade)
		{
			registerModelSpade(item);
		}
	}
	
	// Pickaxes
	
	@SideOnly(Side.CLIENT)
	private void registerModelPickaxe(ItemPickaxe item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(new ResourceLocation(Reference.MOD_ID, item.getUnlocalizedName().substring(5)), "inventory"));
	}
	
	public List<ItemPickaxe> getItemPickaxe()
	{
		return pa;
	}
	
	// Hoe
	
	@SideOnly(Side.CLIENT)
	private void registerModelHoe(ItemHoe item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(new ResourceLocation(Reference.MOD_ID, item.getUnlocalizedName().substring(5)), "inventory"));
	}
	
	public List<ItemHoe> getItemHoe()
	{
		return hoe;
	}	
	
	// Axe
	
	@SideOnly(Side.CLIENT)
	private void registerModelAxe(ItemAxe item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(new ResourceLocation(Reference.MOD_ID, item.getUnlocalizedName().substring(5)), "inventory"));
	}
	
	public List<ItemAxe> getItemAxe()
	{
		return axe;
	}
	
	// Spades
	
	@SideOnly(Side.CLIENT)
	private void registerModelSpade(ItemSpade item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(new ResourceLocation(Reference.MOD_ID, item.getUnlocalizedName().substring(5)), "inventory"));
	}
	
	public List<ItemSpade> getItemSpade()
	{
		return spade;
	}
	
}
