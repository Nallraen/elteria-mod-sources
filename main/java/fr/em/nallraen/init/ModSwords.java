package fr.em.nallraen.init;

import java.util.List;

import com.google.common.collect.Lists;

import fr.em.nallraen.objects.weapons.EMSwords;
import fr.em.nallraen.util.Reference;
import fr.em.nallraen.util.ToolMaterials;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemSword;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModSwords 
{
	
	
	public static final ModSwords INSTANCE = new ModSwords();
	
	public List<ItemSword> swords;
	
	// Swords
	
	public static ItemSword topaz_sword;
	public static ItemSword obsi_sword;
	public static ItemSword aqua_sword;
	public static ItemSword elterie_sword;
	public static ItemSword amethyst_sword;
	public static ItemSword ruby_sword;
	public static ItemSword pyra_sword;
	public static ItemSword emerald_sword;
	public static ItemSword saphir_sword;
	public static ItemSword ura_sword;
	public static ItemSword tour_sword;
	// Kits royaumes
	public static ItemSword angel_sword;
	public static ItemSword demon_sword;
	
	public void init()
	{
		
		swords = Lists.newArrayList();
		
		// Swords
		
		topaz_sword = new EMSwords("topaz_sword", ToolMaterials.topazMat);
		obsi_sword = new EMSwords("obsi_sword", ToolMaterials.obsiMat);
		aqua_sword = new EMSwords("aqua_sword", ToolMaterials.aquaMat);
		elterie_sword = new EMSwords("elterite_sword", ToolMaterials.elteriteMat);
		amethyst_sword = new EMSwords("amethyst_sword", ToolMaterials.amethystMat);
		ruby_sword = new EMSwords("ruby_sword", ToolMaterials.rubyMat);
		pyra_sword = new EMSwords("pyra_sword", ToolMaterials.pyraMat);
		emerald_sword = new EMSwords("emerald_sword", ToolMaterials.emeraldMat);
		saphir_sword = new EMSwords("saphir_sword", ToolMaterials.saphirMat);
		ura_sword = new EMSwords("ura_sword", ToolMaterials.uraMat);
		tour_sword = new EMSwords("tour_sword", ToolMaterials.tourMat);
		// Kits royaumes
		angel_sword = new EMSwords("angel_sword", ToolMaterials.angelMat);
		demon_sword = new EMSwords("demon_sword", ToolMaterials.demonMat);
		
		
	}
	
	
	@SubscribeEvent
	public void registerModels(ModelRegistryEvent e)
	{
		for(ItemSword item : swords)
		{
			registerModel(item);
		}
	}
	
	
	@SideOnly(Side.CLIENT)
	private void registerModel(ItemSword item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(new ResourceLocation(Reference.MOD_ID, item.getUnlocalizedName().substring(5)), "inventory"));
	}
	
	public List<ItemSword> getItemSword()
	{
		return swords;
	}
	
}
