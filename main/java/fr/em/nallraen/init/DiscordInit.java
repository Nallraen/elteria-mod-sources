package fr.em.nallraen.init;

import net.arikia.dev.drpc.DiscordEventHandlers;
import net.arikia.dev.drpc.DiscordRPC;
import net.arikia.dev.drpc.DiscordRichPresence;

public class DiscordInit 
{
	
	private static String DiscordID = "701860337632149616";
	
	public void init()
	{
		DiscordEventHandlers handlers = new DiscordEventHandlers.Builder().setReadyEventHandler((user) -> {
			System.out.println("Discord Rich Prensence has been setup, thank you " + user.username + "#" + user.discriminator + " to use our mod.");
			DiscordRichPresence.Builder p = new DiscordRichPresence.Builder("Selecting a mode.");
			p.setDetails("Elteria Main Menu");
		}).build();
		DiscordRPC.discordInitialize(DiscordID, handlers, false);
		DiscordRPC.discordRegister(DiscordID, "");
		
		
		/*
		 * 
		 * DiscordRPC 
		 * 
		 * TODO: Create an activation handler to potentially deactivate DiscordRPC InGame.
		 * 
		 */
	
		
	}
	
}
