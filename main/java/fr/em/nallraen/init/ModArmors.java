package fr.em.nallraen.init;

import java.util.List;

import com.google.common.collect.Lists;

import fr.em.nallraen.objects.armors.EMArmor;
import fr.em.nallraen.util.Reference;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModArmors
{

	public static final ModArmors INSTANCE = new ModArmors();
	
	public List<ItemArmor> armors;
	
	// Armors
	
	
	// Pyrargyrite Armor 
	
	public static ItemArmor pyra_helmet;
	public static ItemArmor pyra_chest;
	public static ItemArmor pyra_legg;
	public static ItemArmor pyra_boots;
	
	public static ItemArmor amethyst_helmet;
	public static ItemArmor amethyst_chest;
	public static ItemArmor amethyst_legg;
	public static ItemArmor amethyst_boots;
	
	public static ItemArmor aqua_helmet;
	public static ItemArmor aqua_chest;
	public static ItemArmor aqua_legg;
	public static ItemArmor aqua_boots;
	
	public static ItemArmor emerald_helmet;
	public static ItemArmor emerald_chest;
	public static ItemArmor emerald_legg;
	public static ItemArmor emerald_boots;
	
	public static ItemArmor obsi_helmet;
	public static ItemArmor obsi_chest;
	public static ItemArmor obsi_legg;
	public static ItemArmor obsi_boots;
	
	public static ItemArmor ruby_helmet;
	public static ItemArmor ruby_chest;
	public static ItemArmor ruby_legg;
	public static ItemArmor ruby_boots;
	
	public static ItemArmor saphir_helmet;
	public static ItemArmor saphir_chest;
	public static ItemArmor saphir_legg;
	public static ItemArmor saphir_boots;
	
	public static ItemArmor topaz_helmet;
	public static ItemArmor topaz_chest;
	public static ItemArmor topaz_legg;
	public static ItemArmor topaz_boots;
	
	public static ItemArmor tourmaline_helmet;
	public static ItemArmor tourmaline_chest;
	public static ItemArmor tourmaline_legg;
	public static ItemArmor tourmaline_boots;
	
	public static ItemArmor uranium_helmet;
	public static ItemArmor uranium_chest;
	public static ItemArmor uranium_legg;
	public static ItemArmor uranium_boots;

 	public static ItemArmor elterite_helmet;
	public static ItemArmor elterite_chest;
	public static ItemArmor elterite_legg;
	public static ItemArmor elterite_boots;
	
	public static ItemArmor demon_helmet;
	public static ItemArmor demon_chest;
	public static ItemArmor demon_legg;
	public static ItemArmor demon_boots;
	
	public static ItemArmor angel_helmet;
	public static ItemArmor angel_chest;
	public static ItemArmor angel_legg;
	public static ItemArmor angel_boots;
	
	// Amethyst Armor
	
	
	
	
	public void init()
	{
		armors = Lists.newArrayList();
		
		// Armors
		
		// Pyrargyrite
		pyra_helmet = new EMArmor("pyra_helmet", ArmorMaterials.pyraMat, 2, EntityEquipmentSlot.HEAD);
		pyra_chest = new EMArmor("pyra_chest", ArmorMaterials.pyraMat, 2, EntityEquipmentSlot.CHEST);
		pyra_legg = new EMArmor("pyra_legg", ArmorMaterials.pyraMat, 2, EntityEquipmentSlot.LEGS);
		pyra_boots = new EMArmor("pyra_boots", ArmorMaterials.pyraMat, 2, EntityEquipmentSlot.FEET);
		
		// Amethyst
		amethyst_helmet = new EMArmor("amethyst_helmet", ArmorMaterials.amethystMat, 2, EntityEquipmentSlot.HEAD);
		amethyst_chest = new EMArmor("amethyst_chest", ArmorMaterials.amethystMat, 2, EntityEquipmentSlot.CHEST);
		amethyst_legg = new EMArmor("amethyst_legg", ArmorMaterials.amethystMat, 2, EntityEquipmentSlot.LEGS);
		amethyst_boots = new EMArmor("amethyst_boots", ArmorMaterials.amethystMat, 2, EntityEquipmentSlot.FEET);
		
		// Aquamarine 
		aqua_helmet = new EMArmor("aqua_helmet", ArmorMaterials.aquaMat, 2, EntityEquipmentSlot.HEAD);
		aqua_chest = new EMArmor("aqua_chest", ArmorMaterials.aquaMat, 2, EntityEquipmentSlot.CHEST);
		aqua_legg = new EMArmor("aqua_legg", ArmorMaterials.aquaMat, 2, EntityEquipmentSlot.LEGS);
		aqua_boots = new EMArmor("aqua_boots", ArmorMaterials.aquaMat, 2, EntityEquipmentSlot.FEET);
		
		
		// Emerald
		emerald_helmet = new EMArmor("emerald_helmet", ArmorMaterials.emeraldMat, 2, EntityEquipmentSlot.HEAD);
		emerald_chest = new EMArmor("emerald_chest", ArmorMaterials.emeraldMat, 2, EntityEquipmentSlot.CHEST);
		emerald_legg = new EMArmor("emerald_legg", ArmorMaterials.emeraldMat, 2, EntityEquipmentSlot.LEGS);
		emerald_boots = new EMArmor("emerald_boots", ArmorMaterials.emeraldMat, 2, EntityEquipmentSlot.FEET);
		
		
		// Obsidian
		obsi_helmet = new EMArmor("obsi_helmet", ArmorMaterials.obsiMat, 2, EntityEquipmentSlot.HEAD);
		obsi_chest = new EMArmor("obsi_chest", ArmorMaterials.obsiMat, 2, EntityEquipmentSlot.CHEST);
		obsi_legg = new EMArmor("obsi_legg", ArmorMaterials.obsiMat, 2, EntityEquipmentSlot.LEGS);
		obsi_boots = new EMArmor("obsi_boots", ArmorMaterials.obsiMat, 2, EntityEquipmentSlot.FEET);
		
		
		// Ruby
		ruby_helmet = new EMArmor("ruby_helmet", ArmorMaterials.rubyMat, 2, EntityEquipmentSlot.HEAD);
		ruby_chest = new EMArmor("ruby_chest", ArmorMaterials.rubyMat, 2, EntityEquipmentSlot.CHEST);
		ruby_legg = new EMArmor("ruby_legg", ArmorMaterials.rubyMat, 2, EntityEquipmentSlot.LEGS);
		ruby_boots = new EMArmor("ruby_boots", ArmorMaterials.rubyMat, 2, EntityEquipmentSlot.FEET);
		
		
		// Saphir
		saphir_helmet = new EMArmor("saphir_helmet", ArmorMaterials.saphirMat, 2, EntityEquipmentSlot.HEAD);
		saphir_chest = new EMArmor("saphir_chest", ArmorMaterials.saphirMat, 2, EntityEquipmentSlot.CHEST);
		saphir_legg = new EMArmor("saphir_legg", ArmorMaterials.saphirMat, 2, EntityEquipmentSlot.LEGS);
		saphir_boots = new EMArmor("saphir_boots", ArmorMaterials.saphirMat, 2, EntityEquipmentSlot.FEET);
		
		
		// Topaz
		topaz_helmet = new EMArmor("topaz_helmet", ArmorMaterials.topazMat, 2, EntityEquipmentSlot.HEAD);
		topaz_chest = new EMArmor("topaz_chest", ArmorMaterials.topazMat, 2, EntityEquipmentSlot.CHEST);
		topaz_legg = new EMArmor("topaz_legg", ArmorMaterials.topazMat, 2, EntityEquipmentSlot.LEGS);
		topaz_boots = new EMArmor("topaz_boots", ArmorMaterials.topazMat, 2, EntityEquipmentSlot.FEET);
		
		
		// Tourmaline
		tourmaline_helmet = new EMArmor("tourmaline_helmet", ArmorMaterials.tourMat, 2, EntityEquipmentSlot.HEAD);
		tourmaline_chest = new EMArmor("tourmaline_chest", ArmorMaterials.tourMat, 2, EntityEquipmentSlot.CHEST);
		tourmaline_legg = new EMArmor("tourmaline_legg", ArmorMaterials.tourMat, 2, EntityEquipmentSlot.LEGS);
		tourmaline_boots = new EMArmor("tourmaline_boots", ArmorMaterials.tourMat, 2, EntityEquipmentSlot.FEET);
		
		
		// Uranium
		uranium_helmet = new EMArmor("uranium_helmet", ArmorMaterials.uraMat, 2, EntityEquipmentSlot.HEAD);
		uranium_chest = new EMArmor("uranium_chest", ArmorMaterials.uraMat, 2, EntityEquipmentSlot.CHEST);
		uranium_legg = new EMArmor("uranium_legg", ArmorMaterials.uraMat, 2, EntityEquipmentSlot.LEGS);
		uranium_boots = new EMArmor("uranium_boots", ArmorMaterials.uraMat, 2, EntityEquipmentSlot.FEET);

		// Elterite
		elterite_helmet = new EMArmor("elterite_helmet", ArmorMaterials.elteriteMat, 2, EntityEquipmentSlot.HEAD);
		elterite_chest = new EMArmor("elterite_chest", ArmorMaterials.elteriteMat, 2, EntityEquipmentSlot.CHEST);
		elterite_legg = new EMArmor("elterite_legg", ArmorMaterials.elteriteMat, 2, EntityEquipmentSlot.LEGS);
		elterite_boots = new EMArmor("elterite_boots", ArmorMaterials.elteriteMat, 2, EntityEquipmentSlot.FEET);

		// Demon
		demon_helmet = new EMArmor("demon_helmet", ArmorMaterials.demonMat, 2, EntityEquipmentSlot.HEAD);
		demon_chest = new EMArmor("demon_chest", ArmorMaterials.demonMat, 2, EntityEquipmentSlot.CHEST);
		demon_legg = new EMArmor("demon_legg", ArmorMaterials.demonMat, 2, EntityEquipmentSlot.LEGS);
		demon_boots = new EMArmor("demon_boots", ArmorMaterials.demonMat, 2, EntityEquipmentSlot.FEET);

		// Kits royaumes
		// Angel
		angel_helmet = new EMArmor("angel_helmet", ArmorMaterials.angelMat, 2, EntityEquipmentSlot.HEAD);
		angel_chest = new EMArmor("angel_chest", ArmorMaterials.angelMat, 2, EntityEquipmentSlot.CHEST);
		angel_legg = new EMArmor("angel_legg", ArmorMaterials.angelMat, 2, EntityEquipmentSlot.LEGS);
		angel_boots = new EMArmor("angel_boots", ArmorMaterials.angelMat, 2, EntityEquipmentSlot.FEET);
		
	}
	
	// Les materiaux d'armures.
	public static class ArmorMaterials
	{
		
		/* La durabilité : 
		 * Casque = x*11
		 * Chest = x*16
		 * Jambes = x*15
		 * Bottes = x*13
		 */
		
		public static final ItemArmor.ArmorMaterial obsiMat = EnumHelper.addArmorMaterial("obsiMat", Reference.MOD_ID+":obsi_armor", 20, new int[] {1, 2, 4, 1}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		public static final ItemArmor.ArmorMaterial emeraldMat = EnumHelper.addArmorMaterial("emeraldMat", Reference.MOD_ID+":emerald_armor", 25, new int[] {2, 4, 5, 1}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		public static final ItemArmor.ArmorMaterial topazMat = EnumHelper.addArmorMaterial("topazMat", Reference.MOD_ID+":topaz_armor", 34, new int[] {3, 6, 8, 3}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		public static final ItemArmor.ArmorMaterial rubyMat = EnumHelper.addArmorMaterial("rubyMat", Reference.MOD_ID+":ruby_armor", 35, new int[] {3, 6, 8, 3}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		public static final ItemArmor.ArmorMaterial uraMat = EnumHelper.addArmorMaterial("uraMat", Reference.MOD_ID+":uranium_armor", 36, new int[] {3, 6, 8, 3}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		public static final ItemArmor.ArmorMaterial saphirMat = EnumHelper.addArmorMaterial("saphirMat", Reference.MOD_ID+":saphir_armor", 37, new int[] {3, 6, 8, 3}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		public static final ItemArmor.ArmorMaterial elteriteMat = EnumHelper.addArmorMaterial("elteriteMat", Reference.MOD_ID+":elterite_armor", 38, new int[] {3, 6, 8, 3}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		public static final ItemArmor.ArmorMaterial pyraMat = EnumHelper.addArmorMaterial("pyraMat", Reference.MOD_ID+":pyra_armor", 39, new int[] {2,5, 7, 2}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		public static final ItemArmor.ArmorMaterial aquaMat = EnumHelper.addArmorMaterial("aquaMat", Reference.MOD_ID+":aquamarine_armor", 40, new int[] {2, 5,7, 2}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		public static final ItemArmor.ArmorMaterial tourMat = EnumHelper.addArmorMaterial("tourMat", Reference.MOD_ID+":tourmaline_armor", 41, new int[] {2, 5, 7, 2}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		public static final ItemArmor.ArmorMaterial amethystMat = EnumHelper.addArmorMaterial("amethystMat", Reference.MOD_ID+":amethyst_armor", 42, new int[] {2, 5, 7, 2}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		// Kits royaume
		public static final ItemArmor.ArmorMaterial demonMat = EnumHelper.addArmorMaterial("demonMat", Reference.MOD_ID+":demon_armor", 20, new int[] {1, 2, 4, 1}, 1, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
		public static final ItemArmor.ArmorMaterial angelMat = EnumHelper.addArmorMaterial("angelMat", Reference.MOD_ID+":angel_armor", 20, new int[] {1, 2, 4, 1}, 1, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);
	}
	
	
	
	@SubscribeEvent
	public void registerModels(ModelRegistryEvent e)
	{
		for(ItemArmor item : armors)
		{
			registerModel(item);
		}
	}
	
	
	@SideOnly(Side.CLIENT)
	private void registerModel(ItemArmor item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(new ResourceLocation(Reference.MOD_ID, item.getUnlocalizedName().substring(5)), "inventory"));
	}
	
	public List<ItemArmor> getItemArmor()
	{
		return armors;
	}
	
}
