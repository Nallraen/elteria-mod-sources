package fr.em.nallraen.init;

import java.util.List;

import com.google.common.collect.Lists;

import fr.em.nallraen.util.Reference;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;

public class ModSounds 
{

	public static final ModSounds INSTANCE = new ModSounds();
	
	public static List<SoundEvent> sounds;

	public static final SoundEvent kronor = new SoundEvent(new ResourceLocation(Reference.MOD_ID, "kronor")).setRegistryName("kronor");
	public static final SoundEvent atshandia = new SoundEvent(new ResourceLocation(Reference.MOD_ID, "atshandia")).setRegistryName("atshandia");
	public static final SoundEvent cannibale = new SoundEvent(new ResourceLocation(Reference.MOD_ID, "cannibale")).setRegistryName("cannibale");
	public static final SoundEvent krovor = new SoundEvent(new ResourceLocation(Reference.MOD_ID, "krovor")).setRegistryName("krovor");
	public static final SoundEvent lypsia = new SoundEvent(new ResourceLocation(Reference.MOD_ID, "lypsia")).setRegistryName("lypsia");
	public static final SoundEvent noel = new SoundEvent(new ResourceLocation(Reference.MOD_ID, "noel")).setRegistryName("noel");
	public static final SoundEvent royaumeanges = new SoundEvent(new ResourceLocation(Reference.MOD_ID, "royaumeanges")).setRegistryName("royaumeanges");
	public static final SoundEvent shandia = new SoundEvent(new ResourceLocation(Reference.MOD_ID, "shandia")).setRegistryName("shandia");
	public static final SoundEvent spawn = new SoundEvent(new ResourceLocation(Reference.MOD_ID, "spawn")).setRegistryName("spawn");
	public static final SoundEvent warzone = new SoundEvent(new ResourceLocation(Reference.MOD_ID, "warzone")).setRegistryName("warzone");
	public static final SoundEvent shop = new SoundEvent(new ResourceLocation(Reference.MOD_ID, "shop")).setRegistryName("shop");
	
	/*
	 * ======================================
	 * 		  ON VERRA A MODIF CE CODE
	 * ======================================
	 */
	
	
	public void init()
	{
		sounds = Lists.newArrayList();
		
		// Sounds 
		
	}
	
	public List<SoundEvent> getSounds()
	{
		return sounds;
	}
	
}
