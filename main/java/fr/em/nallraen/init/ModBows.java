package fr.em.nallraen.init;

import java.util.List;

import com.google.common.collect.Lists;

import fr.em.nallraen.objects.weapons.EMBows;
import fr.em.nallraen.util.Reference;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemBow;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModBows 
{
	public static final ModBows INSTANCE = new ModBows();
	
	public List<ItemBow> bows;
	
	// Bows
	public static ItemBow obsi_bow;
	public static ItemBow emerald_bow;
	public static ItemBow diamond_bow;
	public static ItemBow ruby_bow;
	public static ItemBow topaz_bow;
	public static ItemBow ura_bow;
	public static ItemBow saphir_bow;
	public static ItemBow elterite_bow;
	public static ItemBow pyra_bow;
	public static ItemBow aqua_bow;
	public static ItemBow tour_bow;
	public static ItemBow amethyst_bow;
	
	
	public void init()
	{
		bows = Lists.newArrayList();
		
		/* Bows
		 * Ordre des arcs : 
		 * Obsidienne > Emeraude > Diamant > Rubis > Topaze > Uranium > Saphir > Elterite > Pyrargyrite > Aquamarine > Tourmaline > Améthyste
		 */

		obsi_bow = new EMBows("obsi_bow", 800);
		emerald_bow = new EMBows("emerald_bow", 900);
		diamond_bow = new EMBows("diamond_bow", 1000);
		ruby_bow = new EMBows("ruby_bow", 1100);
		topaz_bow = new EMBows("topaz_bow", 1200);
		ura_bow = new EMBows("ura_bow", 1300);
		saphir_bow = new EMBows("saphir_bow", 1400);
		elterite_bow = new EMBows("elterite_bow", 1500);
		pyra_bow = new EMBows("pyra_bow", 1600);
		aqua_bow = new EMBows("aqua_bow", 1700);
		tour_bow = new EMBows("tour_bow", 1800);
		amethyst_bow = new EMBows("amethyst_bow", 2000);
	}
	
	
	@SubscribeEvent
	public void registerModels(ModelRegistryEvent e)
	{
		for(ItemBow ib : bows)
		{
			registerModel(ib);
		}
	}
	
	
	@SideOnly(Side.CLIENT)
	private void registerModel(ItemBow item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(new ResourceLocation(Reference.MOD_ID, item.getUnlocalizedName().substring(5)), "inventory"));
	}
	
	public List<ItemBow> getItemBow()
	{
		return bows;
	}
}
