package fr.em.nallraen.init;

import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModRecipes 
{
	
	public static ModRecipes INSTANCE = new ModRecipes();
	
	public void initRecipes()
	{
		GameRegistry.addSmelting(Blocks.OBSIDIAN, new ItemStack(ModItems.obsidian_ingot, 2), 50.0f);
		GameRegistry.addSmelting(ModBlocks.amethyst_ore, new ItemStack(ModItems.amethyst, 1), 50.0f);
		GameRegistry.addSmelting(ModBlocks.aqua_ore, new ItemStack(ModItems.aquamarine, 1), 50.0f);
		GameRegistry.addSmelting(ModBlocks.elterite_ore, new ItemStack(ModItems.elterite, 1), 50.0f);
		GameRegistry.addSmelting(ModBlocks.pyra_ore, new ItemStack(ModItems.pyrargyrite, 1), 50.0f);
		GameRegistry.addSmelting(ModBlocks.ruby_ore, new ItemStack(ModItems.ruby, 1), 50.0f);
		GameRegistry.addSmelting(ModBlocks.saphir_ore, new ItemStack(ModItems.saphir, 1), 50.0f);
		GameRegistry.addSmelting(ModBlocks.topaz_ore, new ItemStack(ModItems.topaz, 1), 50.0f);
		GameRegistry.addSmelting(ModBlocks.tourmaline_ore, new ItemStack(ModItems.tourmaline, 1), 50.0f);
		GameRegistry.addSmelting(ModBlocks.ura_ore, new ItemStack(ModItems.uranium, 1), 50.0f);
	}
}
