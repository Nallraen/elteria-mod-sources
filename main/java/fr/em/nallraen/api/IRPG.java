package fr.em.nallraen.api;

import fr.em.nallraen.api.jobs.IRPGJobs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

public interface IRPG 
{

	void setConstitution(EntityPlayer player, int amt);
	
	void setStrength(int amt);
	
	void setDefense(int amt);
	
	void setDexterity(EntityPlayer player, int amt);
	
	void setIntelligence(int amt);
	
	void setExperienceSaved(int amt);
	
	void setMagic(int amt);
	
	void setJob(EntityPlayer player, IRPGJobs job);
	
	int getConstitution();
	
	int getStrength();
	
	int getDefense();
	
	int getDexterity();
	
	int getIntellgence();
	
	int getExperienceSaved();
	
	int getLevel();
	
	int getMagic();
	
	int getMaxMagic();
	
	IRPGJobs getJob();
	
	NBTTagCompound saveNBT();

    void loadNBT(EntityPlayer player, NBTTagCompound nbt);
	
}
