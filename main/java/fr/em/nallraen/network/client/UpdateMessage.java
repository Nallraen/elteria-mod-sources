package fr.em.nallraen.network.client;

import java.io.IOException;

import fr.em.nallraen.api.IRPG;
import fr.em.nallraen.api.jobs.capabilities.RPGCapability.CapabilityProvider;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.relauncher.Side;
import palaster.libpal.network.AbstractMessage;

public class UpdateMessage extends AbstractMessage.AbstractClientMessage<UpdateMessage>
{


	private NBTTagCompound tag = null;
	
	public UpdateMessage() {}

    public UpdateMessage(NBTTagCompound tag) { this.tag = tag; }

	@Override
	protected void read(PacketBuffer buffer) throws IOException { tag = ByteBufUtils.readTag(buffer); }

	@Override
	protected void write(PacketBuffer buffer) throws IOException { ByteBufUtils.writeTag(buffer, tag); }

	@Override
	protected void process(EntityPlayer player, Side side) {
		if(player != null && tag != null) {
			final IRPG rpg = CapabilityProvider.get(player);
			if(rpg != null)
				rpg.loadNBT(player, tag);
		}
	}
}
