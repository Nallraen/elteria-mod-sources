package fr.em.nallraen.objects.weapons;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.init.ModBows;
import net.minecraft.init.Items;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemStack;

public class EMBows extends ItemBow 
{

	public EMBows(String name, int maxDamage)
	{
		setRegistryName(name).setUnlocalizedName(name);
		setCreativeTab(ElteriaMain.modtab);
		setMaxDamage(maxDamage);
		setMaxStackSize(1);
		
		ModBows.INSTANCE.getItemBow().add(this);
		
	}
	
	@Override
	protected boolean isArrow(ItemStack stack)
	{
		if(stack.getItem() == Items.ARROW || stack.getItem() == Items.SPECTRAL_ARROW || stack.getItem() == Items.TIPPED_ARROW)
		{
			return true;
		}
		return false;
	}
	
	@Override
	public int getItemEnchantability()
	{
		return 10;
	}
	
}
