package fr.em.nallraen.objects.weapons;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.init.ModBows;
import fr.em.nallraen.init.ModSwords;
import net.minecraft.item.ItemSword;

public class EMSwords extends ItemSword {

	public EMSwords(String name, ToolMaterial material) {
		super(material);
		setRegistryName(name).setUnlocalizedName(name);
		setCreativeTab(ElteriaMain.modtab);
		
		ModSwords.INSTANCE.getItemSword().add(this);
	}

}
