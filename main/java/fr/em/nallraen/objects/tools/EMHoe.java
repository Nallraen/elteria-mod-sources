package fr.em.nallraen.objects.tools;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.init.ModTools;
import net.minecraft.item.ItemHoe;

public class EMHoe extends ItemHoe {

	public EMHoe(String name, ToolMaterial material) {
		super(material);

		setRegistryName(name).setUnlocalizedName(name);
		setCreativeTab(ElteriaMain.modtab);
		
		ModTools.INSTANCE.getItemHoe().add(this);

	}

}
