package fr.em.nallraen.objects.tools;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.init.ModTools;
import net.minecraft.item.ItemAxe;

public class EMAxe extends ItemAxe {

	public EMAxe(String name, ToolMaterial material, float damage, float speed) {
		super(material, damage, speed);
		

		setRegistryName(name).setUnlocalizedName(name);
		setCreativeTab(ElteriaMain.modtab);
	
		
		ModTools.INSTANCE.getItemAxe().add(this);

	}

}
