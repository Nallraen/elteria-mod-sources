package fr.em.nallraen.objects.tools;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.init.ModTools;
import net.minecraft.item.ItemPickaxe;

public class EMPickAxe extends ItemPickaxe {

	public EMPickAxe(String name, ToolMaterial material) 
	{
			
		super(material);

		setRegistryName(name).setUnlocalizedName(name);
		setCreativeTab(ElteriaMain.modtab);
		
		ModTools.INSTANCE.getItemPickaxe().add(this);
	
	}

}
