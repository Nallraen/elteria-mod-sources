package fr.em.nallraen.objects.tools;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.init.ModTools;
import net.minecraft.item.ItemSpade;

public class EMSpade extends ItemSpade {

	public EMSpade(String name, ToolMaterial material) {
		super(material);


		setRegistryName(name).setUnlocalizedName(name);
		setCreativeTab(ElteriaMain.modtab);
		
		ModTools.INSTANCE.getItemSpade().add(this);
	}

}
