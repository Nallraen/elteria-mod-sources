package fr.em.nallraen.objects.items;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.api.IRPG;
import fr.em.nallraen.api.jobs.capabilities.RPGCapability.CapabilityDefault;
import fr.em.nallraen.api.jobs.capabilities.RPGCapability.CapabilityProvider;
import fr.em.nallraen.core.proxy.CommonProxy;
import fr.em.nallraen.init.ModItems;
import fr.em.nallraen.util.Reference;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import palaster.libpal.api.IReceiveButton;

public class EMRpgItem extends Item
					   implements IReceiveButton
{

	public EMRpgItem(String name)
	{
		setRegistryName(name).setUnlocalizedName(name);
		setCreativeTab(ElteriaMain.modtab);
		setMaxStackSize(64);

		ModItems.INSTANCE.getItems().add(this);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World wIn, EntityPlayer playerIn, EnumHand hand)
	{
		if(!wIn.isRemote && !playerIn.getHeldItem(hand).isEmpty())
		{
			final IRPG rpg = CapabilityProvider.get(playerIn);
			if(rpg != null)
			{
				CommonProxy.syncPlayerCapabilitiesToClient(playerIn);
				playerIn.openGui(ElteriaMain.class, 1, wIn, 0, 0, hand == EnumHand.MAIN_HAND ? 0 : 1);
				return ActionResult.newResult(EnumActionResult.SUCCESS, playerIn.getHeldItem(hand));
			}
		}
		return super.onItemRightClick(wIn, playerIn, hand);
	}
	
	public void receiveButtonEvent(int buttonId, EntityPlayer player)
	{
		final IRPG rpg = CapabilityProvider.get(player);
		if(rpg != null)
		{
			switch(buttonId)
			{
				case 0: 
				{
					if(player.experienceLevel >= CapabilityDefault.getExperienceCostForNextLevel(player) && rpg.getConstitution() < CapabilityDefault.MAX_LEVEL)
					{
						if(player.experienceLevel - CapabilityDefault.getExperienceCostForNextLevel(player) <= 0)
							player.addExperienceLevel(-player.experienceLevel);
						else if(CapabilityDefault.getExperienceCostForNextLevel(player) > 0)
							player.addExperienceLevel(-CapabilityDefault.getExperienceCostForNextLevel(player));
						rpg.setConstitution(player, rpg.getConstitution() + 1);
						rpg.setExperienceSaved(0);
						CommonProxy.syncPlayerCapabilitiesToClient(player);
					}
					break;
				}
				case 1: 
				{
					if(player.experienceLevel >= CapabilityDefault.getExperienceCostForNextLevel(player) && rpg.getStrength() < CapabilityDefault.MAX_LEVEL)
					{
						if(player.experienceLevel - CapabilityDefault.getExperienceCostForNextLevel(player) <= 0)
							player.addExperienceLevel(-player.experienceLevel);
						else if(CapabilityDefault.getExperienceCostForNextLevel(player) > 0)
							player.addExperienceLevel(-CapabilityDefault.getExperienceCostForNextLevel(player));
						rpg.setStrength(rpg.getStrength() + 1);
						rpg.setExperienceSaved(0);
						CommonProxy.syncPlayerCapabilitiesToClient(player);
					}
					break;
				}
				case 2:
				{
					if(player.experienceLevel >= CapabilityDefault.getExperienceCostForNextLevel(player) && rpg.getDefense() < CapabilityDefault.MAX_LEVEL)
					{
						if(player.experienceLevel - CapabilityDefault.getExperienceCostForNextLevel(player) <= 0)
							player.addExperienceLevel(-player.experienceLevel);
						else if(CapabilityDefault.getExperienceCostForNextLevel(player) > 0)
							player.addExperienceLevel(-CapabilityDefault.getExperienceCostForNextLevel(player));
						rpg.setDefense(rpg.getDefense() + 1);
						rpg.setExperienceSaved(0);
						CommonProxy.syncPlayerCapabilitiesToClient(player);
					}
					break;
				}
				case 3:
				{
					if(player.experienceLevel >= CapabilityDefault.getExperienceCostForNextLevel(player) && rpg.getDexterity() < CapabilityDefault.MAX_LEVEL)
					{
						if(player.experienceLevel - CapabilityDefault.getExperienceCostForNextLevel(player) <= 0)
							player.addExperienceLevel(-player.experienceLevel);
						else if(CapabilityDefault.getExperienceCostForNextLevel(player) > 0) 
							player.addExperienceLevel(-CapabilityDefault.getExperienceCostForNextLevel(player));
						rpg.setDexterity(player, rpg.getDexterity() + 1);
						rpg.setExperienceSaved(0);
						CommonProxy.syncPlayerCapabilitiesToClient(player);
					}
					break;
				}
				case 4:
				{
					if(player.experienceLevel >= CapabilityDefault.getExperienceCostForNextLevel(player) && rpg.getIntellgence() < CapabilityDefault.MAX_LEVEL)
					{
						if(player.experienceLevel - CapabilityDefault.getExperienceCostForNextLevel(player) <= 0)
							player.addExperienceLevel(-player.experienceLevel);
						else if(CapabilityDefault.getExperienceCostForNextLevel(player) > 0)
							player.addExperienceLevel(-CapabilityDefault.getExperienceCostForNextLevel(player));
						rpg.setIntelligence(rpg.getIntellgence() + 1);
						rpg.setExperienceSaved(0);
						CommonProxy.syncPlayerCapabilitiesToClient(player);
					}
					break;
				}
				case 5:
				{
					if(player.experienceLevel >= 1 && CapabilityDefault.getExperienceCostForNextLevel(player) > 1)
					{
						player.addExperienceLevel(-1);
						rpg.setExperienceSaved(rpg.getExperienceSaved() + 1);
						CommonProxy.syncPlayerCapabilitiesToClient(player);
					}
					break;
				}
			}
		}
	}
	
}
