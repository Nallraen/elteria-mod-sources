package fr.em.nallraen.objects.items;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.init.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class EMItems extends Item 
{

	public EMItems(String name)
	{
		setRegistryName(name).setUnlocalizedName(name);
		setCreativeTab(ElteriaMain.modtab);
		setMaxStackSize(64);
		
		ModItems.INSTANCE.getItems().add(this);
	}
	
	
}
