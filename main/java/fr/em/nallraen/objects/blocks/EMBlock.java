package fr.em.nallraen.objects.blocks;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.init.ModBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class EMBlock 
	   extends Block 
{

	public EMBlock(String name, Material materialIn) {
		super(materialIn);
		setRegistryName(name).setUnlocalizedName(name);
		setCreativeTab(ElteriaMain.modtab);
		
		
		ModBlocks.INSTANCE.getBlocks().add(this);
	}
	
	// hardness = dur�e de minage ; resistance = resistance � l'explosion !
	public EMBlock(String name, Material materialIn, float hardness, float resistance)
	{
		this(name, materialIn);
		
		
		setHardness(hardness);
		setResistance(resistance);
		setCreativeTab(ElteriaMain.modtab);
	}
	
	public EMBlock(String name, Material material, float hardness, float resistance, int harvestLevel, String harvestType)
	{
		this(name, material, hardness, resistance);
		
		setHarvestLevel(harvestType, harvestLevel);
		setCreativeTab(ElteriaMain.modtab);
		
		/* Harvest Level : 
		 * 0 -> Bois
		 * 1 -> Pierre
		 * 2 -> Fer
		 * 3 -> Diamant 
		 * 
		 * Harvest Type : 
		 * Pickaxe 
		 * Axe
		 * Shovel / Spade ? 
		 */
		
	}


}
