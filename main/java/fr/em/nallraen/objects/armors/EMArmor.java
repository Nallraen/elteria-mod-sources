package fr.em.nallraen.objects.armors;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.init.ModArmors;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;

public class EMArmor extends ItemArmor {

	public EMArmor(String name, ArmorMaterial materialIn, int renderIndexIn, EntityEquipmentSlot equipmentSlotIn) {
		super(materialIn, renderIndexIn, equipmentSlotIn);

		setRegistryName(name).setUnlocalizedName(name);
		setCreativeTab(ElteriaMain.modtab);
		
		ModArmors.INSTANCE.getItemArmor().add(this);
		
	}

}
