package fr.em.nallraen.util;

import net.minecraft.util.ResourceLocation;

public class Reference 
{
	
	// Mod Infos
	
	public static final String MOD_ID = "em",
			VERSION = "2.0",
			MC_VERSION = "[1.12.2]",
			NAME = "Elteria Mod v2",
			DEPENDENCIES = "required-after:libpal";

	// Proxy
	
	public static final String CLIENT_PROXY_CLASS = "fr.em.nallraen.core.proxy.ClientProxy";
	public static final String COMMON_PROXY_CLASS = "fr.em.nallraen.core.proxy.CommonProxy";
	public static final String SERVER_PROXY_CLASS = "fr.em.nallraen.core.proxy.ServerProxy";

	// Discord Settings
	
	public static final String DiscordID = "701860337632149616";

	// RPG Settings
	
	private static final String GUI = "textures/gui/";
	private static final String MODELS = "textures/models/";
	
	public static final ResourceLocation BLANK = new ResourceLocation(MOD_ID, GUI + "blank.png");
	
	public static final ResourceLocation SPINE_SHOOTER = new ResourceLocation(MOD_ID, MODELS + "spine_shooter.png");

}
