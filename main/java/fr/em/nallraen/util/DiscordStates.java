package fr.em.nallraen.util;

import java.time.Instant;

import net.arikia.dev.drpc.DiscordRPC;
import net.arikia.dev.drpc.DiscordRichPresence;
import net.minecraft.client.Minecraft;

public class DiscordStates {
	
	public static long inGameStartTime;
	
	public static void inMenu()
	{
		DiscordRichPresence.Builder p = new DiscordRichPresence.Builder("Just Chillin'");
		
		p.setDetails("In Menu");
		DiscordRPC.discordUpdatePresence(p.build());
		System.out.println("######################################");
		System.out.println("######################################");
		System.out.println("######################################");
		System.out.println(" UPDATE DISCORDRPC WITH THE MAIN MENU ");
		System.out.println("######################################");
		System.out.println("######################################");
		System.out.println("######################################");
	}
	
	public static void inGame()
	{
		boolean isSingleplayer = Minecraft.getMinecraft().isSingleplayer();
		boolean isOffline = Minecraft.getMinecraft().isIntegratedServerRunning();
		String ign = Minecraft.getMinecraft().player.getDisplayNameString();
		String gamemode = Minecraft.getMinecraft().playerController.getCurrentGameType().getName();

		
		DiscordRichPresence.Builder drp = new DiscordRichPresence.Builder("Playing in " + gamemode + " mode");
		DiscordRPC drpc = new DiscordRPC();
		
		drp.setDetails("Pseudo: " + ign);
		if(isOffline == true)
		{
			if(isSingleplayer == true)
			{
				drp.setBigImage("sp", "Playing Solo on: To be Updated..."); // + Minecraft.getMinecraft().getSaveLoader().getWorldInfo(Minecraft.getMinecraft().getSaveLoader().getName()).getWorldName());
			}
		} else 
		{
			String server = Minecraft.getMinecraft().getCurrentServerData().serverIP.toString();
			if(server.equalsIgnoreCase("play.elteria.ovh"))
			{
				drp.setBigImage("mp", "Playing Multiplayer on: " + server);
			} else if(server.equalsIgnoreCase("play.elteria.ovh:25572"))
			{
				drp.setBigImage("mp", "Playing Multiplayer on: play.elteria.ovh");
			} else
			{
				drp.setBigImage("mp", "Playing Multiplayer on: " + server);
			}
			
			//drp.setBigImage("mp", "Playing Mutiplayer on: "+ Minecraft.getMinecraft().getCurrentServerData().serverIP.substring(0,6));
		}
		drp.setSmallImage("copyright", "Made with love by Nallraen");
		
		if(inGameStartTime == 0)
		{
			Instant instant = Instant.now();
			inGameStartTime = instant.getEpochSecond();
		}
		drp.setStartTimestamps(inGameStartTime);
		drpc.discordUpdatePresence(drp.build());
	}

}
