package fr.em.nallraen.util;

import fr.em.nallraen.init.ModTools;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;

public class ToolMaterials 
{
    // HarvestLevel : 0 -> Bois, 1 -> Pierre, 2 -> Fer, 3 -> Diamant
	// D�gat �p�e = x+4
	// (Plus Faible) Obsidienne > Emeraude > Diamant > Rubis > Topaze > Uranium > Saphir 
	//               > Elterite > Pyrargyrite > Aquamarine > Tourmaline > Am�thyste (Plus Fort)
	public static final ToolMaterial obsiMat = EnumHelper.addToolMaterial("obsiMat", 3, 1337, 8.0f, 2.5f, 10);
	public static final ToolMaterial emeraldMat = EnumHelper.addToolMaterial("emeraldMat", 3, 1550, 8.0f, 2.5f, 10);
	public static final ToolMaterial topazMat = EnumHelper.addToolMaterial("topazMat", 3, 1591, 8.0f, 4.0f, 10);
	public static final ToolMaterial rubyMat = EnumHelper.addToolMaterial("rubyMat", 3, 1601, 8.0f, 4.2f, 10);
	public static final ToolMaterial uraMat = EnumHelper.addToolMaterial("uraMat", 3, 1631, 8.0f, 5.2f, 10);
	public static final ToolMaterial saphirMat = EnumHelper.addToolMaterial("saphirMat", 3, 1661, 8.0f, 5.2f, 10);
	public static final ToolMaterial elteriteMat = EnumHelper.addToolMaterial("elteriteMat", 3, 1691, 8.0f, 5.4f, 10);
	public static final ToolMaterial pyraMat = EnumHelper.addToolMaterial("pyraMat", 3, 1712, 8.0f, 5.5f, 10);
	public static final ToolMaterial aquaMat = EnumHelper.addToolMaterial("aquaMat", 3, 1712, 8.0f, 5.7f, 10);
	public static final ToolMaterial tourMat = EnumHelper.addToolMaterial("tourMat", 3, 1730, 8f, 5.7f, 10);
	public static final ToolMaterial amethystMat = EnumHelper.addToolMaterial("amethystMat", 3, 1731, 8.0f, 5.9f, 10);
	public static final ToolMaterial angelMat = EnumHelper.addToolMaterial("angelMat", 3, 1605, 6.0f, 1.7f, 0);
	public static final ToolMaterial demonMat = EnumHelper.addToolMaterial("demonMat", 3, 1605, 6.0f, 1.7f, 0);

}