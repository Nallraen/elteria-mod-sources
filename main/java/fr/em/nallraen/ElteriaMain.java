package fr.em.nallraen;

import fr.em.nallraen.api.IRPG;
import fr.em.nallraen.core.proxy.CommonProxy;
import fr.em.nallraen.events.DiscordEvents;
import fr.em.nallraen.events.RegisteringEvent;
import fr.em.nallraen.init.DiscordInit;
import fr.em.nallraen.tabs.EMTab;
import fr.em.nallraen.user.commands.RPGGuiCommand;
import fr.em.nallraen.util.DiscordStates;
import fr.em.nallraen.util.Reference;
import fr.em.nallraen.world.gen.WorldOreGen;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = Reference.MOD_ID, version = Reference.VERSION, name = Reference.NAME, dependencies = Reference.DEPENDENCIES)
public class ElteriaMain 
{
	
	DiscordInit di = new DiscordInit();
	DiscordEvents de = new DiscordEvents();

	 @Instance
	 public static ElteriaMain instance;
	
	 @SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
	 public static CommonProxy proxy;
	 
	 public static final CreativeTabs modtab = new EMTab("emtabs");

	 public ElteriaMain()
	 {
		 MinecraftForge.EVENT_BUS.register(new RegisteringEvent());
	 }
	 
	 
	 
	 @Mod.EventHandler
	 public void preInit(FMLPreInitializationEvent event)
	 {
		 proxy.preInit();
		 GameRegistry.registerWorldGenerator(new WorldOreGen(), 4);
		 
	 }
	 
	 @Mod.EventHandler
	 public void init(FMLInitializationEvent event)
	 {
		 proxy.Init();
		 di.init();
	 }
	 
	 @Mod.EventHandler
	 public void postInit(FMLPostInitializationEvent event)
	 {
		 DiscordStates.inMenu();
	 }
	 
	 @Mod.EventHandler
	 public void serverInit(FMLServerStartingEvent event)
	 {
		 proxy.Init();
		 event.registerServerCommand(new RPGGuiCommand());
	 }
		
	 
	/*/ Discord Event States 
	 @EventBusSubscriber
	 public static class EventHandlers
	 {
		 @SubscribeEvent
		 public static void Tick(TickEvent.RenderTickEvent e)
		 {
			 DiscordRPC.discordRunCallbacks();
		 }
		 
		 @SubscribeEvent
		 public static void LogIn(TickEvent.PlayerTickEvent e)
		 {
			 
			 if(tickCounter < 250)
			 {
				 tickCounter++;
			 } else
			 {
				 DiscordStates.inGame();
				 tickCounter = 0;
			 }
		 }
		 
		 @SubscribeEvent
		 public static void LogOut(PlayerEvent.PlayerLoggedOutEvent e)
		 {
			 DiscordStates.inMenu();
		 }
	 }*/
	 
}
