package fr.em.nallraen.tabs;

import fr.em.nallraen.init.ModBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class EMTab extends CreativeTabs {

	public EMTab(String label)
	{
		super(label);
	}
	
	
	@Override
	public ItemStack getTabIconItem() {
		return new ItemStack(ModBlocks.ruby_block);
	}

}
