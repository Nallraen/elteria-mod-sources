package fr.em.nallraen.world.gen;

import java.util.Random;

import fr.em.nallraen.init.ModBlocks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldOreGen implements IWorldGenerator
{

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) 
	{
		// TODO Auto-generated method stub
		if(world.getWorldType().equals(WorldType.DEFAULT))
		{
			generateOverworld(random, chunkX, chunkZ, world, chunkGenerator, chunkProvider);
		}
	}
	
	private void generateOverworld(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider)
	{
		generateOreOverworld(ModBlocks.ruby_ore.getDefaultState(), world, random, chunkX * 16, chunkZ * 16, 20, 50, random.nextInt(5) + 1, 15);
		generateOreOverworld(ModBlocks.topaz_ore.getDefaultState(), world, random, chunkX * 16, chunkZ * 16, 20, 50, random.nextInt(5) + 1, 10);
		generateOreOverworld(ModBlocks.ura_ore.getDefaultState(), world, random, chunkX * 16, chunkZ * 16, 2, 25, random.nextInt(5) + 1, 5);
		generateOreOverworld(ModBlocks.saphir_ore.getDefaultState(), world, random, chunkX * 16, chunkZ * 16, 1, 50, random.nextInt(5) + 1, 15);
		generateOreOverworld(ModBlocks.elterite_ore.getDefaultState(), world, random, chunkX * 16, chunkZ * 16, 1, 50, random.nextInt(5) + 1, 10);
		generateOreOverworld(ModBlocks.pyra_ore.getDefaultState(), world, random, chunkX * 16, chunkZ * 16, 1, 50, random.nextInt(5) + 1, 5);
		generateOreOverworld(ModBlocks.aqua_ore.getDefaultState(), world, random, chunkX * 16, chunkZ * 16, 1, 30, random.nextInt(5) + 1, 15);
		generateOreOverworld(ModBlocks.tourmaline_ore.getDefaultState(), world, random, chunkX * 16, chunkZ * 16, 1, 30, random.nextInt(5) + 1, 10);
		generateOreOverworld(ModBlocks.amethyst_ore.getDefaultState(), world, random, chunkX * 16, chunkZ * 16, 1, 20, random.nextInt(5) + 1, 5);
	}
	
	private void generateOreOverworld(IBlockState ore, World world, Random random, int x, int z, int minY, int maxY, int size, int chances) 
    {
        int deltaY = maxY - minY;
        
        for (int i = 0; i < chances; i++) 
        {
            BlockPos pos = new BlockPos(x + random.nextInt(16), minY + random.nextInt(deltaY), z + random.nextInt(16));
            
            WorldGenMinable generator = new WorldGenMinable(ore, size);
            generator.generate(world, random, pos);
        }
    }
	
	
	
}
