package fr.em.nallraen.gui;

import java.io.IOException;
import java.lang.ref.WeakReference;

import fr.em.nallraen.api.IRPG;
import fr.em.nallraen.api.jobs.capabilities.RPGCapability.CapabilityDefault;
import fr.em.nallraen.api.jobs.capabilities.RPGCapability.CapabilityProvider;
import fr.em.nallraen.inventories.ContainerRPGIntro;
import fr.em.nallraen.util.Reference;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import palaster.libpal.network.PacketHandler;
import palaster.libpal.network.server.GuiButtonMessage;

@SideOnly(Side.CLIENT)
public class GuiRPGIntro extends GuiContainer
{
	private WeakReference<EntityPlayer> player = null;
	private int hand = -1;
	
	public GuiRPGIntro(EntityPlayer player, int hand)
	{
		super(new ContainerRPGIntro());
		this.player = new WeakReference<EntityPlayer>(player);
		this.hand = hand;
		ySize = 160;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		mc.renderEngine.bindTexture(Reference.BLANK);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		if(player != null && player.get() != null)
		{
			//TODO: R�cup�rer les valeurs du joueur ! 
			final IRPG rpg = CapabilityProvider.get(player.get());
			if(rpg != null)
			{
				if(rpg.getJob() != null)
					fontRenderer.drawString("Job: " + rpg.getJob().getJobName(), 6, 6, 4210752);
				else
					fontRenderer.drawString("Level: " + rpg.getLevel(), 6, 16, 4210752);
					fontRenderer.drawString("Job: Vous n'avez pas de job", 6, 6, 4210752);
				fontRenderer.drawString("Constitution: " + rpg.getConstitution(), 6, 26, 4210752);
				fontRenderer.drawString("Strength: " + rpg.getStrength(), 6, 36, 4210752);
				fontRenderer.drawString("Defense: " + rpg.getDefense(), 6, 46, 4210752);
				fontRenderer.drawString("Dexterity: " + rpg.getDexterity(), 6, 56, 4210752);
				fontRenderer.drawString("Intelligence: " + rpg.getIntellgence(), 6, 66, 4210752);
				if(rpg.getJob() == null || !rpg.getJob().replaceMagic())
					fontRenderer.drawString("Magic: " + rpg.getMagic() + "/" + rpg.getMaxMagic(), 98, 66, 4210752);
				if(CapabilityDefault.getExperienceCostForNextLevel(player.get()) > player.get().experienceLevel)
					fontRenderer.drawString("Exp cost: " + CapabilityDefault.getExperienceCostForNextLevel(player.get()) +"", 6, 76, 0x8A0707);
				else
					fontRenderer.drawString("Exp cost: " + CapabilityDefault.getExperienceCostForNextLevel(player.get()) +"", 6, 76, 0x009900);
				if(rpg.getJob() != null)
					rpg.getJob().drawExtraInformationBase(player.get(), fontRenderer, 6, 86, mouseX, mouseY);
			}
			/*TODO: Le gui ne s'affiche pas, faire une requ�te pour placer le joueur automatiquement !
			 * 				28/05/2020 - Arr�t ici... 
			 *	- Capability Finies,
			 *	- Modifs IRPG
			 *  - Pr�paration des EventHandler
			 *  - Finitions du menu (Sur cette class)
			 *  
			 */
			
			//fontRenderer.drawString("Job: Vous n'avez pas de job", 6, 6, 4210752);
		}
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		buttonList.clear();
		
		buttonList.add(new GuiButton(0, guiLeft + 82, guiTop + 26, 12, 10, "->"));
		buttonList.add(new GuiButton(1, guiLeft + 82, guiTop + 36, 12, 10, "->"));
		buttonList.add(new GuiButton(2, guiLeft + 82, guiTop + 46, 12, 10, "->"));
		buttonList.add(new GuiButton(3, guiLeft + 82, guiTop + 56, 12, 10, "->"));
		buttonList.add(new GuiButton(4, guiLeft + 82, guiTop + 66, 12, 10, "->"));
		buttonList.add(new GuiButton(5, guiLeft + 82, guiTop + 76, 12, 10, "->"));
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException 
	{
		if (player != null && player.get() != null)
		{
			PacketHandler.sendToServer(new GuiButtonMessage(hand, player.get().getPosition(), button.id));
		}
	}
	
}
