package fr.em.nallraen.events;

import net.arikia.dev.drpc.DiscordEventHandlers;
import net.arikia.dev.drpc.DiscordRichPresence;

public class DiscordEvents 
{

	public DiscordEventHandlers handlers = new DiscordEventHandlers.Builder().setReadyEventHandler((user) -> {
		System.out.println("Discord Rich Prensence has been setup, thank you " + user.username + "#" + user.discriminator + " to use our mod.");
		DiscordRichPresence.Builder p = new DiscordRichPresence.Builder("Selecting a mode.");
		p.setDetails("Elteria Main Menu");
		p.setBigImage("logo", "Elteria");
	}).build();
	
	
}
