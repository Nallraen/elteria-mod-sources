package fr.em.nallraen.events;

import fr.em.nallraen.api.IRPG;
import fr.em.nallraen.api.jobs.capabilities.RPGCapability.CapabilityProvider;
import fr.em.nallraen.init.RPGItems;
import fr.em.nallraen.util.Reference;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickItem;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;

@Mod.EventBusSubscriber(modid = Reference.MOD_ID)
public class EventHandler 
{

	// Jobs related events
	
	@SubscribeEvent
	public void onBreakSpeed(BreakSpeed e)
	{
		//TODO: BreakSpeed
	}
	
	@SubscribeEvent
	public void onPlayerTick(PlayerTickEvent e)
	{
		//TODO: PlayerTickEvent
	}
	
	@SubscribeEvent
	public void onPlayerInteract(RightClickItem e)
	{
		//TODO: PlyaerInteract
	}
	
	@SubscribeEvent
	public void onHarvestDrops(HarvestDropsEvent e)
	{
		//TODO: HarvesterEvent
	}
	
	@SubscribeEvent
	public void onLivingHurt(LivingHurtEvent e)
	{
		//TODO: LivingHurt Event...
	}
	
	// End of features
	
	// Start of common caps
	
	@SubscribeEvent
	public void attachEntityCapability(AttachCapabilitiesEvent<Entity> e) {
		if(e.getObject() instanceof EntityPlayer && e.getObject() != null && !e.getObject().hasCapability(CapabilityProvider.RPG_CAP, null))
			e.addCapability(new ResourceLocation("test", ":IRPG"), new CapabilityProvider((EntityPlayer) e.getObject()));
	}
	
	@SubscribeEvent
	public void onClonePlayer(PlayerEvent.Clone e)
	{
		if(!e.getEntityPlayer().world.isRemote)
		{
			final IRPG rpgOG = CapabilityProvider.get(e.getOriginal());
			final IRPG rpgNew = CapabilityProvider.get(e.getEntityPlayer());
			if(rpgOG != null && rpgNew != null)
				rpgNew.loadNBT(e.getEntityPlayer(), rpgOG.saveNBT());
		}
	}
}
