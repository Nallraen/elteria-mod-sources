package fr.em.nallraen.events;

import fr.em.nallraen.init.ModArmors;
import fr.em.nallraen.init.ModBlocks;
import fr.em.nallraen.init.ModBows;
import fr.em.nallraen.init.ModItems;
import fr.em.nallraen.init.ModSounds;
import fr.em.nallraen.init.ModSwords;
import fr.em.nallraen.init.ModTools;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemSword;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickItem;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;

@Mod.EventBusSubscriber
public class RegisteringEvent 
{
	
	// Base registering Events
	
	
	@SubscribeEvent
	public void registerItems(RegistryEvent.Register<Item> e) throws Exception
	{
		ModItems.INSTANCE.init();
		e.getRegistry().registerAll(ModItems.INSTANCE.getItems().toArray(new Item[0]));
		ModBows.INSTANCE.init();
		e.getRegistry().registerAll(ModBows.INSTANCE.getItemBow().toArray(new ItemBow[0]));
		ModArmors.INSTANCE.init();
		e.getRegistry().registerAll(ModArmors.INSTANCE.getItemArmor().toArray(new ItemArmor[0]));
		ModSwords.INSTANCE.init();
		e.getRegistry().registerAll(ModSwords.INSTANCE.getItemSword().toArray(new ItemSword[0]));
		ModTools.INSTANCE.init();
		e.getRegistry().registerAll(ModTools.INSTANCE.getItemAxe().toArray(new ItemAxe[0]));
		e.getRegistry().registerAll(ModTools.INSTANCE.getItemHoe().toArray(new ItemHoe[0]));
		e.getRegistry().registerAll(ModTools.INSTANCE.getItemPickaxe().toArray(new ItemPickaxe[0]));
		e.getRegistry().registerAll(ModTools.INSTANCE.getItemSpade().toArray(new ItemSpade[0]));
		
	}
	
	
	@SubscribeEvent
	public void registerBlocks(RegistryEvent.Register<Block> e)
	{
		ModBlocks.INSTANCE.init();
		e.getRegistry().registerAll(ModBlocks.INSTANCE.getBlocks().toArray(new Block[0]));
	}
	
	@SubscribeEvent
	public void registerSounds(RegistryEvent.Register<SoundEvent> e)
	{
		e.getRegistry().register(ModSounds.INSTANCE.kronor);
		e.getRegistry().register(ModSounds.INSTANCE.atshandia);
		e.getRegistry().register(ModSounds.INSTANCE.cannibale);
		e.getRegistry().register(ModSounds.INSTANCE.krovor);
		e.getRegistry().register(ModSounds.INSTANCE.lypsia);
		e.getRegistry().register(ModSounds.INSTANCE.noel);
		e.getRegistry().register(ModSounds.INSTANCE.royaumeanges);
		e.getRegistry().register(ModSounds.INSTANCE.shandia);
		e.getRegistry().register(ModSounds.INSTANCE.spawn);
		e.getRegistry().register(ModSounds.INSTANCE.shop);
		e.getRegistry().register(ModSounds.INSTANCE.warzone);
		
		// e.getRegistry().registerAll(ModSounds.INSTANCE.getSounds().toArray(new SoundEvent[0]));
	}
	
}
