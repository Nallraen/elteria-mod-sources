package fr.em.nallraen.jobs;

import java.util.HashMap;

import javax.annotation.Nullable;

import fr.em.nallraen.api.EnumDomain;
import fr.em.nallraen.api.jobs.IRPGJobs;
import fr.em.nallraen.jobs.abilities.GodPowers;
import net.minecraft.nbt.NBTTagCompound;

public class JobGod implements IRPGJobs
{

	public static final String TAG_STRING_DOMAIN = "em:GodDomain",
			TAG_MAP_ACTIVE_POWERS = "em:GodActivePower";

	private EnumDomain domain = null;
	private HashMap<String, Boolean> godPowers = new HashMap<>();

	public JobGod() {
		for(String godPower : GodPowers.GOD_POWERS)
			godPowers.put(godPower, false);
	}

	public void setDomain(EnumDomain domain) { this.domain = domain; }

	@Nullable
	public EnumDomain getDomain() { return domain; }

	public boolean isPowerActive(String power) { return godPowers.getOrDefault(power, false); }

	public void setPower(String power, boolean activate) { godPowers.put(power, activate); }

	@Override
	public String getJobName() { return "em.job.god"; }

	@Override
	public boolean canLeave() { return false; }

	@Override
	public boolean replaceMagic() { return true; }

	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound nbt = new NBTTagCompound();
		if(domain != null)
			nbt.setString(TAG_STRING_DOMAIN, domain.toString());
		if(!godPowers.isEmpty())
			for(String power : godPowers.keySet())
				nbt.setBoolean(TAG_MAP_ACTIVE_POWERS + "_" + power, godPowers.getOrDefault(power, false));
		return nbt;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		if(nbt.hasKey(TAG_STRING_DOMAIN))
			domain = EnumDomain.valueOf(nbt.getString(TAG_STRING_DOMAIN));
		if(!godPowers.isEmpty())
			for(String power : godPowers.keySet())
				godPowers.put(power, nbt.getBoolean(TAG_MAP_ACTIVE_POWERS + "_" + power));
	}
	
}
