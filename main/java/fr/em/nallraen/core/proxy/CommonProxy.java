package fr.em.nallraen.core.proxy;

import javax.annotation.Nullable;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.api.IRPG;
import fr.em.nallraen.api.jobs.capabilities.RPGCapability.CapabilityFactory;
import fr.em.nallraen.api.jobs.capabilities.RPGCapability.CapabilityProvider;
import fr.em.nallraen.api.jobs.capabilities.RPGCapability.CapabilityStorage;
import fr.em.nallraen.events.EventHandler;
import fr.em.nallraen.events.RegisteringEvent;
import fr.em.nallraen.gui.GuiRPGIntro;
import fr.em.nallraen.init.ModRecipes;
import fr.em.nallraen.inventories.ContainerRPGIntro;
import fr.em.nallraen.network.client.UpdateMessage;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import palaster.libpal.network.PacketHandler;

public class CommonProxy implements IGuiHandler
{

	public void preInit()
	{
		
		PacketHandler.registerMessage(UpdateMessage.class);
		CapabilityManager.INSTANCE.register(IRPG.class, new CapabilityStorage(), new CapabilityFactory());
		
		
		/*
		System.out.println("===================================");
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! PreInit Client");
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT ITEMS");
		MinecraftForge.EVENT_BUS.register(ModItems.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT BLOCKS");
		MinecraftForge.EVENT_BUS.register(ModBlocks.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT BOWS");
		MinecraftForge.EVENT_BUS.register(ModBows.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT ARMORS");
		MinecraftForge.EVENT_BUS.register(ModArmors.INSTANCE);
		System.out.println("[Elteria Mod v2] ! PreInit Client DONE");
		System.out.println("===================================");
		System.out.println("===================================");
 		*/
	}

	public void Init()
	{
		ModRecipes.INSTANCE.initRecipes();
		
		//MinecraftForge.EVENT_BUS.register(new EventHandler());
		//NetworkRegistry.INSTANCE.registerGuiHandler(ElteriaMain.instance, this);
	}

	public void postInit()
	{
	}
	
	public static void syncPlayerCapabilitiesToClient(@Nullable EntityPlayer player)
	{
		if(player != null && !player.world.isRemote)
			if(CapabilityProvider.get(player) != null)
				PacketHandler.sendTo(new UpdateMessage(CapabilityProvider.get(player).saveNBT()), (EntityPlayerMP) player);
	}

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		switch(ID) 
		{
			case 1: {
				return new ContainerRPGIntro();
			}
		}
		return null;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		
		switch(ID) {
			case 1: {
				return new GuiRPGIntro(player, z);
			}
		}
		
		return null;
	}

}
