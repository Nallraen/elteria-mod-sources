package fr.em.nallraen.core.proxy;

import fr.em.nallraen.init.ModArmors;
import fr.em.nallraen.init.ModBlocks;
import fr.em.nallraen.init.ModBows;
import fr.em.nallraen.init.ModItems;
import fr.em.nallraen.init.ModSounds;
import fr.em.nallraen.init.ModSwords;
import fr.em.nallraen.init.ModTools;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;

public class ServerProxy extends CommonProxy 
{

	@Override
	public void preInit()
	{
		super.preInit();
		/*
		super.preInit();
		System.out.println("===================================");
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! PreInit Server");
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT ITEMS");
		MinecraftForge.EVENT_BUS.register(ModItems.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT BLOCKS");
		MinecraftForge.EVENT_BUS.register(ModBlocks.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT BOWS");
		MinecraftForge.EVENT_BUS.register(ModBows.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT ARMORS");
		MinecraftForge.EVENT_BUS.register(ModArmors.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT SWORDS");
		MinecraftForge.EVENT_BUS.register(ModSwords.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT TOOLS");
		MinecraftForge.EVENT_BUS.register(ModTools.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT SOUNDS");
		MinecraftForge.EVENT_BUS.register(ModSounds.INSTANCE);
		System.out.println("[Elteria Mod v2] ! PreInit SERVER DONE");
		System.out.println("===================================");
		System.out.println("===================================");
		*/
	}
	
	@Override
	public void Init() { super.Init(); }
	
	@Override
	public void postInit() { super.postInit(); }
	
}
