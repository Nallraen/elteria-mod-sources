package fr.em.nallraen.core.proxy;

import fr.em.nallraen.ElteriaMain;
import fr.em.nallraen.events.DiscordStatesEvent;
import fr.em.nallraen.events.EventHandler;
import fr.em.nallraen.gui.MainMenu;
import fr.em.nallraen.init.MenuInit;
import fr.em.nallraen.init.ModArmors;
import fr.em.nallraen.init.ModBlocks;
import fr.em.nallraen.init.ModBows;
import fr.em.nallraen.init.ModItems;
import fr.em.nallraen.init.ModSounds;
import fr.em.nallraen.init.ModSwords;
import fr.em.nallraen.init.ModTools;
import fr.em.nallraen.util.DiscordStates;
import net.arikia.dev.drpc.DiscordRPC;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

public class ClientProxy extends CommonProxy 
{

	@Override
	public void preInit()
	{
		super.preInit();
		System.out.println("===================================");
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! PreInit Client");
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT ITEMS");
		MinecraftForge.EVENT_BUS.register(ModItems.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT BLOCKS");
		MinecraftForge.EVENT_BUS.register(ModBlocks.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT BOWS");
		MinecraftForge.EVENT_BUS.register(ModBows.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT ARMORS");
		MinecraftForge.EVENT_BUS.register(ModArmors.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT SWORDS");
		MinecraftForge.EVENT_BUS.register(ModSwords.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT TOOLS");
		MinecraftForge.EVENT_BUS.register(ModTools.INSTANCE);
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT SOUNDS");
		MinecraftForge.EVENT_BUS.register(ModSounds.INSTANCE);
		System.out.println("[Elteria Mod v2] ! PreInit Client DONE");
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT DiscordRPC");
		System.out.println("===================================");
		System.out.println("===================================");
	}
	
	@Override
	public void Init() 
	{
		super.Init(); 
		
		System.out.println("===================================");
		System.out.println("[Elteria Mod v2] ! INIT DISCORD");
		// MinecraftForge.EVENT_BUS.register(DiscordStatesEvent.INSTANCE);
		System.out.println("====================================");
		
		System.out.println("[Elteria Mod v2] ! INIT MAIN MENU");
		MinecraftForge.EVENT_BUS.register(MenuInit.INSTANCE);
		
		
		MinecraftForge.EVENT_BUS.register(new EventHandler());
		NetworkRegistry.INSTANCE.registerGuiHandler(ElteriaMain.instance, this);
	
	}
	
	@Override
	public void postInit() { super.postInit(); }

}

