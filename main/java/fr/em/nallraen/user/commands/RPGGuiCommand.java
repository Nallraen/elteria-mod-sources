package fr.em.nallraen.user.commands;

import fr.em.nallraen.ElteriaMain;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;

public class RPGGuiCommand extends CommandBase
{

	/*
	@Override
	public void execute(EntityPlayer p, ICommandSender s, World worldIn, EnumHand hand) throws CommandException
	{
		p.openGui(ElteriaMain.instance, 1, worldIn, 0, 0, hand == EnumHand.MAIN_HAND ? 0 : 1);
	} */
	
	@Override
	public String getName()
	{
		return "rpg-gui";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/rpg-gui";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		// TODO Auto-generated method stub
		EntityPlayer player = (EntityPlayer) sender.getCommandSenderEntity();
		World w = sender.getEntityWorld();
		player.openGui(ElteriaMain.instance, 1, w, 0, 0, 0);
	} 
	
}
